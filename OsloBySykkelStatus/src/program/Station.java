package program;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONObject;

public class Station {
	
	private int stationId,
				capacity,
				numBikesAvailable,
				numDocksAvailable;
	private String name,
				   address;
	private Map<String,String> rentalUris = new HashMap<String, String>();
	private double lat, 
				   lon;
	private boolean isInstalled, 
					isRenting, 
					isReturning;
	private long lastReported; 
	
	Station(JSONObject station, JSONObject status) {
		stationId = station.getInt("station_id");
		name = station.getString("name");
		address = station.getString("address");
		
		Iterator<?> keys = station.getJSONObject("rental_uris").keys();
		rentalUris.put((String) keys.next(), station.getJSONObject("rental_uris").getString("android"));
		rentalUris.put((String) keys.next(), station.getJSONObject("rental_uris").getString("ios"));
		
		lat = station.getDouble("lat");
		lon = station.getDouble("lon");
		capacity = station.getInt("capacity");
		
		isInstalled = status.getInt("is_installed") == 1;
		isRenting = status.getInt("is_renting") == 1;
		isReturning = status.getInt("is_returning") == 1;
		lastReported = status.getLong("last_reported");
		numBikesAvailable = status.getInt("num_bikes_available");
		numDocksAvailable = status.getInt("num_docks_available");
	}

	@Override
	public String toString() {
		return "Station [station_id=" + stationId + ", capacity=" + capacity + ", num_bikes_available="
				+ numBikesAvailable + ", num_docks_available=" + numDocksAvailable + ", name=" + name + ", address="
				+ address + ", rental_uris=" + rentalUris + ", lat=" + lat + ", lon=" + lon + ", is_installed="
				+ isInstalled + ", is_renting=" + isRenting + ", is_returning=" + isReturning + ", last_reported="
				+ lastReported + "]";
	}

	public int getStationId() {
		return stationId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Map<String, String> getRentalUris() {
		return new HashMap<String, String>(rentalUris);
	}

	public void setRentalUris(Map<String, String> rentalUris) {
		this.rentalUris = new HashMap<String, String>(rentalUris);
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public boolean isIsInstalled() {
		return isInstalled;
	}

	public void setIsInstalled(boolean isInstalled) {
		this.isInstalled = isInstalled;
	}

	public boolean isIsRenting() {
		return isRenting;
	}

	public void setIsRenting(boolean isRenting) {
		this.isRenting = isRenting;
	}

	public boolean isIsReturning() {
		return isReturning;
	}

	public void setIsReturning(boolean isReturning) {
		this.isReturning = isReturning;
	}

	public int getNumBikesAvailable() {
		return numBikesAvailable;
	}

	public void setNumBikesAvailable(int numBikesAvailable) {
		this.numBikesAvailable = numBikesAvailable;
	}

	public int getNumDocksAvailable() {
		return numDocksAvailable;
	}

	public void setNumDocksAvailable(int numDocksAvailable) {
		this.numDocksAvailable = numDocksAvailable;
	}

	public long getLastReported() {
		return lastReported;
	}
	
	public String getReadableLastReported() {
		LocalDateTime dateTime = LocalDateTime.ofEpochSecond(lastReported, 0, ZoneOffset.UTC);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
		return dateTime.format(formatter);
	}

	public void setLastReported(long lastReported) {
		this.lastReported = lastReported;
	}
}
